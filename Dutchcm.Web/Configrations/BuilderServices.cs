﻿using Dutchcm.Web.Data.Entities;
using Dutchcm.Web.Data;
using Dutchcm.Web.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Dutchcm.Web.Configrations
{
    public static class BuilderServices
    {
        public static void AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            // Add services to the container.
            var connectionString = configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<ApplicationDbContext>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddScoped<ProductRepository>();
            services.AddScoped<ImageRepository>();
            services.AddScoped<ProjectRepository>();
            services.AddScoped<SearchTypeRepository>();
        }

        public static void AddAuthentications(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthentication()
                .AddIdentityServerJwt();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["Jwt:Issuer"],
                    ValidAudience = configuration["Jwt:Issuer"],
                    IssuerSigningKey = new
                    SymmetricSecurityKey
                    (Encoding.UTF8.GetBytes
                    (configuration["Jwt:Key"]))
                };
            });
        }

        public static string AddPolicies(this IServiceCollection services)
        {
            var thirtySecondCachePolicy = "thirtySeconds";

            // Add services to the container.
            services.AddOutputCache((opts) => {
                opts.AddPolicy(thirtySecondCachePolicy, (policy) => policy.Expire(TimeSpan.FromSeconds(30)));
                opts.AddPolicy(thirtySecondCachePolicy, (policy) => policy.With((ctx) => ctx.HttpContext.Request.Query["cache"] == "yes"));
            });

            return thirtySecondCachePolicy;
        }
    }
}
