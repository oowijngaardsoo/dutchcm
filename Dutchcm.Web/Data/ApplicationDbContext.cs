﻿using Duende.IdentityServer.EntityFramework.Options;
using Dutchcm.Web.Data.DataCreation;
using Dutchcm.Web.Data.Entities;
using Dutchcm.Web.Data.Entities.Implementations;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Reflection.Emit;

namespace Dutchcm.Web.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options, operationalStoreOptions)
        {

        }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<SearchType> SearchTypes { get; set; }

        public DbSet<SearchTypeLine> SearchTypesLine { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Project>().HasKey(p => p.Id);
            builder.Entity<Product>().HasKey(p => p.Id);
            base.OnModelCreating(builder);
            Filters(builder);
            CreateSearchTypes.Create(builder);
        }

        private static void Filters(ModelBuilder builder)
        {
            builder.Entity<Project>().HasQueryFilter(p => !p.IsDeleted);
            builder.Entity<Product>().HasQueryFilter(p => !p.IsDeleted);
            builder.Entity<SearchType>().HasQueryFilter(p => !p.IsDeleted);
            builder.Entity<SearchTypeLine>().HasQueryFilter(p => !p.IsDeleted);
        }

        public override int SaveChanges()
        {
            ModifyEntity();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            ModifyEntity();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void ModifyEntity()
        {
            var entries = ChangeTracker
                            .Entries()
                            .Where(e => e.Entity is BaseEntity && (
                                    e.State == EntityState.Added
                                    || e.State == EntityState.Modified)
                                    || e.State == EntityState.Deleted);

            foreach (var entityEntry in entries)
            {
                if (entityEntry.Entity is BaseEntity)
                {
                    if (entityEntry.State == EntityState.Added)
                    {
                        ((BaseEntity)entityEntry.Entity).Created = DateTime.Now;
                        ((BaseEntity)entityEntry.Entity).CreatedBy = string.Empty;
                    }
                    else if (entityEntry.State == EntityState.Modified)
                    {
                        ((BaseEntity)entityEntry.Entity).LastModified = DateTime.Now;
                        ((BaseEntity)entityEntry.Entity).LastModifiedBy = string.Empty;
                    }
                }

                if (entityEntry.Entity is BaseEntityWithSoftDelete)
                {
                    if (entityEntry.State == EntityState.Deleted)
                    {
                        entityEntry.State = EntityState.Modified;
                        ((BaseEntityWithSoftDelete)entityEntry.Entity).DeletedOn = DateTime.Now;
                        ((BaseEntityWithSoftDelete)entityEntry.Entity).IsDeleted = true;
                    }
                }
            }
        }
    }
}