﻿using Dutchcm.Web.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Dutchcm.Web.Data.DataCreation
{
    public static class CreateSearchTypes
    {
        private static List<SearchType> _searchTypes = new List<SearchType>()
        {
            new SearchType()
            {
                Id = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                Name = "Categories",
                Created = DateTime.Parse("1900-01-01"),
                CreatedBy = "",
            },
            new SearchType()
            {
                Id = Guid.Parse("d6d512a4-2805-4d69-a5d9-bb0a7cd815ca"),
                Name = "Materialen",
                Created = DateTime.Parse("1900-01-01"),
                CreatedBy = "",
            },
            new SearchType()
            {
                Id = Guid.Parse("c655e9ef-b7b9-491b-8350-860302c9436e"),
                Name = "Types",
                Created = DateTime.Parse("1900-01-01"),
                CreatedBy = "",
            }
        };

        public static void Create(ModelBuilder builder)
        {
            builder.Entity<SearchType>().HasData(
               _searchTypes
            );

            builder.Entity<SearchTypeLine>().HasData(new List<SearchTypeLine>()
            {
                new SearchTypeLine()
                {
                    Id = Guid.Parse("328159ea-6661-41f8-a301-c0962b9a93b5"),
                    Name = "spijlenhekken",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                },
                new SearchTypeLine()
                {
                    Id = Guid.Parse("8726d0a5-c467-4385-8f01-891bab021f5b"),
                    Name = "strippenhekken",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                },
                new SearchTypeLine()
                {
                    Id = Guid.Parse("8db39e36-71e8-423b-881b-561d04581d25"),
                    Name = "lamellenhekken",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                },
                new SearchTypeLine()
                {
                    Id = Guid.Parse("6cc96883-9185-45ec-8165-bf2c6455bd71"),
                    Name = "glashekken",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                },
                new SearchTypeLine()
                {
                    Id = Guid.Parse("bea1a2a2-01a1-46e0-8f9e-c5e615830c5d"),
                    Name = "stripbalusters",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                },
                new SearchTypeLine()
                {
                    Id = Guid.Parse("5ffd524c-9ee5-49e8-af34-5ac9618e3e27"),
                    Name = "kokerbalusters",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                },
                new SearchTypeLine()
                {
                    Id = Guid.Parse("6b3099c7-0f39-4e70-be58-59c162c36bd3"),
                    Name = "buisbalusters",
                    Created = DateTime.Parse("1900-01-01"),
                    CreatedBy = "",
                    SearchTypeId = Guid.Parse("88506665-a763-4c85-bb58-5209c17ac97c"),
                }
            });

            builder.Entity<SearchTypeLine>().HasData(new List<SearchTypeLine>()
                {
                    new SearchTypeLine()
                    {
                        Id = Guid.Parse("8803c879-f18f-4a1f-9139-8b8d18942b5f"),
                        Name = "RVS",
                        Created = DateTime.Parse("1900-01-01"),
                        CreatedBy = "",
                        SearchTypeId = Guid.Parse("d6d512a4-2805-4d69-a5d9-bb0a7cd815ca"),
                    },
                    new SearchTypeLine()
                    {
                        Id = Guid.Parse("1c23e98c-ddc4-476a-b2e6-75604f953de8"),
                        Name = "Aluminium",
                        Created = DateTime.Parse("1900-01-01"),
                        CreatedBy = "",
                        SearchTypeId = Guid.Parse("d6d512a4-2805-4d69-a5d9-bb0a7cd815ca"),
                    },
                    new SearchTypeLine()
                    {
                        Id = Guid.Parse("8d99d063-fff7-4088-9b35-1cb13f1a4fbb"),
                        Name = "Rubber",
                        Created = DateTime.Parse("1900-01-01"),
                        CreatedBy = "",
                        SearchTypeId = Guid.Parse("d6d512a4-2805-4d69-a5d9-bb0a7cd815ca"),
                    },
                    new SearchTypeLine()
                    {
                        Id = Guid.Parse("7021dc14-56cd-4860-a62b-e11a8ed22352"),
                        Name = "Plastic",
                        Created = DateTime.Parse("1900-01-01"),
                        CreatedBy = "",
                        SearchTypeId = Guid.Parse("d6d512a4-2805-4d69-a5d9-bb0a7cd815ca"),
                    }
                });

            builder.Entity<SearchTypeLine>().HasData(new List<SearchTypeLine>()
                {
                    new SearchTypeLine()
                    {
                        Id = Guid.Parse("538e24bf-e121-4790-a83d-2c7b10d75325"),
                        Name = "HW2000",
                        Created = DateTime.Parse("1900-01-01"),
                        CreatedBy = "",
                        SearchTypeId = Guid.Parse("c655e9ef-b7b9-491b-8350-860302c9436e"),
                    }
                });
        }
    }
}
