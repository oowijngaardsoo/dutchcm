﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Dutchcm.Web.Data.Entities.Implementations;

namespace Dutchcm.Web.Data.Entities
{
    public record Project : BaseEntityWithSoftDelete
    {
        [StringLength(255)]
        [Required]
        public required string Name { get; set; }

        [Required]
        public required string Description { get; set; }

        [Required]
        public DateTime AssertDate { get; set; }

        [StringLength(255)]
        public string? Contractor { get; set; }

        [StringLength(255)]
        public string? Architect { get; set; }

        [StringLength(255)]
        public string? Client { get; set; }

        public Guid? CoverImageId { get; set; }

        [ForeignKey("CoverImageId")]
        public Image? CoverImage { get; set; }

        public ICollection<Image> Images { get; set; } = new List<Image>();
    }
}
