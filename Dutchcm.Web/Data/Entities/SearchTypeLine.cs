﻿using Dutchcm.Web.Data.Entities.Implementations;
using Microsoft.Build.Framework;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dutchcm.Web.Data.Entities
{
    public record SearchTypeLine : BaseEntityWithSoftDelete
    {
        [Required]
        public required string Name { get; set; }

        [Required]
        public Guid SearchTypeId { get; set; }

        [ForeignKey("SearchTypeId")]
        public SearchType SearchType { get; set; }

        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}
