﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Dutchcm.Web.Data.Entities.Implementations;

namespace Dutchcm.Web.Data.Entities
{
    public record Product : BaseEntityWithSoftDelete
    {
        [Required]
        public required string Name { get; set; }

        [Required]
        public required string Description { get; set; }

        [Required]
        public required string Specifications { get; set; }

        public Guid? CoverImageId { get; set; }

        [ForeignKey("CoverImageId")]
        public Image? CoverImage { get; set; }

        public ICollection<Image> Images { get; set; } = new List<Image>();

        public virtual Guid? ProductId { get; set; }

        public bool Ismain { get; set; } = true;

        public ICollection<Product> Products { get; set; } = new List<Product>();

        public ICollection<SearchTypeLine> SearchTypes { get; set; } = new List<SearchTypeLine>();
    }
}
