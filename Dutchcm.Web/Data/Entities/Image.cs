﻿using System.ComponentModel.DataAnnotations;
using Dutchcm.Web.Data.Entities.Implementations;

namespace Dutchcm.Web.Data.Entities
{
    public record Image : BaseEntity
    {
        public Image() : base(Guid.NewGuid())
        {
        }

        [Required]
        public required string FileName { get; set; }

        [Required]
        public required string ImageTitle { get; set; }

        [Required]
        public required string ImageData { get; set; }

        [Required]
        public int Height { get; set; } = 0;

        [Required]
        public int Width { get; set; } = 0;
    }
}
