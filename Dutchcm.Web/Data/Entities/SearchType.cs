﻿using Dutchcm.Web.Data.Entities.Implementations;
using Microsoft.Build.Framework;

namespace Dutchcm.Web.Data.Entities
{
    public record SearchType : BaseEntityWithSoftDelete
    {
        [Required]
        public required string Name { get; set; }

        public ICollection<SearchTypeLine> Lines { get; set; } = new List<SearchTypeLine>();
    }
}
