﻿using System.ComponentModel.DataAnnotations;

namespace Dutchcm.Web.Data.Entities.Implementations
{
    public abstract record BaseEntity : IBaseEntity
    {
        protected BaseEntity(Guid id)
        {
            Id = id;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        public DateTime? LastModified { get; set; }

        public string? LastModifiedBy { get; set; }
    }
}
