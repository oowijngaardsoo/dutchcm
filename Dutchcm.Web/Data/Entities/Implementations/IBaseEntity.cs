﻿namespace Dutchcm.Web.Data.Entities.Implementations
{
    public interface IBaseEntity
    {
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastModified { get; set; }

        public string? LastModifiedBy { get; set; }
    }
}
