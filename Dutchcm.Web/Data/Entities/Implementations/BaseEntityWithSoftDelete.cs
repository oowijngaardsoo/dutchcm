﻿using System.ComponentModel.DataAnnotations;

namespace Dutchcm.Web.Data.Entities.Implementations
{
    public abstract record BaseEntityWithSoftDelete : BaseEntity, IBaseEntityWithSoftDelete
    {
        protected BaseEntityWithSoftDelete() : base(Guid.NewGuid())
        {
        }

        public DateTime? DeletedOn { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
