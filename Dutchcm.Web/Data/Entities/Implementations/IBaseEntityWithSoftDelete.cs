﻿namespace Dutchcm.Web.Data.Entities.Implementations
{
    public interface IBaseEntityWithSoftDelete : IBaseEntity
    {
        public DateTime? DeletedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}
