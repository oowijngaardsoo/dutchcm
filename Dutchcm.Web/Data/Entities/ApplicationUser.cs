﻿using Microsoft.AspNetCore.Identity;

namespace Dutchcm.Web.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {

    }
}