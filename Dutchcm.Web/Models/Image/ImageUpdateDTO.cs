﻿namespace Dutchcm.Web.Models.Image
{
    public class ImageUpdateDTO
    {
        public Guid? Id { get; set; }

        public required IFormFile File { get; set; }

        public bool IsCoverImage { get; set; } = false;
    }
}
