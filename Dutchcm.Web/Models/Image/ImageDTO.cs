﻿namespace Dutchcm.Web.Models.Image
{
    public class ImageDTO
    {
        public Guid? Id { get; set; }

        public required string Data { get; set; }

        public required string ImageTitle { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

    }
}
