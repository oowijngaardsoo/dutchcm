﻿namespace Dutchcm.Web.Models.Requests
{
    public class SearchTypeRequest
    {
        public Guid[]? Ids { get; set; }
    }
}
