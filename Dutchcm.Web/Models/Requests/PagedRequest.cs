﻿namespace Dutchcm.Web.Models
{
    public class PagedRequest : SearchRequest
    {
        public int Page { get; set; } = 1;

        public int Amount { get; set; }
    }

    public class SearchRequest
    {
        public string? SearchString { get; set; }

        public Guid[]? Ids { get; set; }

        public bool useMain { get; set; } = true;
    }
}
