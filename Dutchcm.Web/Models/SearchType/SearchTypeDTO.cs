﻿namespace Dutchcm.Web.Models
{
    public class SearchTypeDTO
    {
        public Guid? Id { get; set; }

        public required string Name { get; set; }

        public List<SearchTypeLineDTO> Lines { get; set; } = new List<SearchTypeLineDTO>();
    }

    public class SearchTypeLineDTO
    {
        public Guid? Id { get; set; }
        public required string Name { get; set; }
    }
}
