﻿using Microsoft.AspNetCore.Mvc;

namespace Dutchcm.Web.Models
{
    public class UpdateResult
    {
        public UpdateResult()
        {
        }

        public UpdateResult(object? result)
        {
            Result = result;
        }

        public Dictionary<string, string> Errors { get;set; } = new Dictionary<string, string>();

        public bool IsSuccess => Errors.Count == 0;

        public object? Result { get; set; }

        public IActionResult? ActionResult { get; set; }
    }
}
