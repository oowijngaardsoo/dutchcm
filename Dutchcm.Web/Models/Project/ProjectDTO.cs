﻿using Dutchcm.Web.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dutchcm.Web.Models.Project
{
    public class ProjectDTO
    {
        public Guid Id { get; set; }

        [StringLength(50)]
        public required string Name { get; set; }

        public required string Description { get; set; }

        public DateTime AssertDate { get; set; }

        [StringLength(50)]
        public string? Contractor { get; set; }

        [StringLength(50)]
        public string? Architect { get; set; }

        [StringLength(50)]
        public string? Client { get; set; }

        public Guid? CoverImageId { get; set; }

        public List<Guid> ImageIds { get; set; } = new List<Guid>();
    }
}
