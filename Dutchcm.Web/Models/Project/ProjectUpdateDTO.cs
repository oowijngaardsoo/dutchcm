﻿using System.ComponentModel.DataAnnotations;

namespace Dutchcm.Web.Models.Project
{
    public class ProjectUpdateDTO
    {
        public Guid? Id { get; set; }

        [StringLength(255)]
        public required string Name { get; set; }

        public required string Description { get; set; }

        public string AssertDate { get; set; }

        [StringLength(255)]
        public string? Contractor { get; set; }

        [StringLength(255)]
        public string? Architect { get; set; }

        [StringLength(255)]
        public string? Client { get; set; }
    }
}
