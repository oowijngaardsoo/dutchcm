﻿namespace Dutchcm.Web.Models.Product
{
    public class ProductDTO
    {
        public Guid Id { get; set; }

        public required string Name { get; set; }

        public required string Description { get; set; }

        public required string Specifications { get; set; }

        public Guid? CoverImageId { get; set; }

        public ICollection<Guid> ImageIds { get; set; } = new List<Guid>();

        public Guid? ProductId { get; set; }

        public bool IsMain { get; set; }

        public List<ProductDTO> Products { get; set; } = new List<ProductDTO>();

        public List<SearchTypeLineDTO> SearchTypes { get; set; } = new List<SearchTypeLineDTO>();
    }
}
