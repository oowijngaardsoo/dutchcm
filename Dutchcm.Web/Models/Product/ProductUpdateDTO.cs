﻿namespace Dutchcm.Web.Models.Product
{
    public class ProductUpdateDTO
    {
        public Guid? Id { get; set; }

        public required string Name { get; set; }

        public required string Description { get; set; }

        public required string Specifications { get; set; }

        public Guid? ProductId { get; set; }


        public List<Guid> SearchTypes { get; set; } = new List<Guid>();
    }
}
