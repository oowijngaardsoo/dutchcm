﻿using Dutchcm.Web.Data;
using Dutchcm.Web.Repositories.Implementations;

namespace Dutchcm.Web.Repositories
{
    public class ProjectRepository : IRepository
    {
        private readonly ApplicationDbContext _context;

        public ProjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }
    }
}
