﻿using Dutchcm.Web.Data;
using Dutchcm.Web.Repositories.Implementations;

namespace Dutchcm.Web.Repositories
{
    public class SearchTypeRepository : IRepository
    {
        private readonly ApplicationDbContext _context;

        public SearchTypeRepository(ApplicationDbContext context)
        {
            _context = context;
        }
    }
}
