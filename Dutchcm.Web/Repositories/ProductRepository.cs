﻿using Dutchcm.Web.Data;
using Dutchcm.Web.Models.Product;
using Dutchcm.Web.Models;
using Dutchcm.Web.Repositories.Implementations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dutchcm.Web.Helpers;
using Dutchcm.Web.Models.Requests;
using Dutchcm.Web.Data.Entities;
using Microsoft.AspNetCore.Http.HttpResults;
using Dutchcm.Web.Models.Image;

namespace Dutchcm.Web.Repositories
{
    public class ProductRepository : IRepository
    {
        private readonly ApplicationDbContext _context;

        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<ProductDTO>> GetProducts(SearchRequest searchRequest)
        {
            var query = _context.Products.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchRequest.SearchString))
            {
                query = query.Where(p =>
                    p.Name.ToLower().Contains(searchRequest.SearchString.ToLower())
                );
            }

            var result = await query
                .Where(p => p.Ismain)
                .OrderBy(s => s.Name)
                .Select(p => new ProductDTO()
                {
                    Name = p.Name,
                    Description= p.Description,
                    Specifications= p.Specifications,
                    CoverImageId= p.CoverImageId,
                    Id= p.Id,
                    IsMain= p.Ismain,
                    ProductId= p.ProductId,
                    ImageIds = p.Images.Select(p => p.Id).ToArray(),
                    SearchTypes = p.SearchTypes.Select(s => new SearchTypeLineDTO() 
                        { 
                            Name = s.Name,
                            Id = s.Id 
                        }
                    ).ToList(),
                })
                .ToListAsync();

            return result;
        }

        public async Task<PagedResult<ProductDTO>> GetProductsPaged(PagedRequest pagedRequest)
        {
            var query = _context.Products
                .AsQueryable();

            if (pagedRequest.Ids != null && pagedRequest.Ids.Length > 0)
            {
                query = query
                    .Include(p => p.CoverImage)
                    .Include(p => p.SearchTypes)
                    .Where(p => p.SearchTypes.Where(s => pagedRequest.Ids.Contains(s.Id)).Any());
            }
            else
            {
                query = query
                    .Include(p => p.CoverImage)
                    .Where(p => p.CoverImageId != null);
            }

            if (pagedRequest.useMain)
            {
                query = query.Where(p => p.Ismain == pagedRequest.useMain);
            }

            var result = await query
                .OrderBy(p => p.Name)
                .Select(p => new ProductDTO
                {
                    Description = p.Description,
                    Name = p.Name,
                    Specifications = p.Specifications,
                    CoverImageId = p.CoverImageId,
                    ProductId = p.ProductId,
                    IsMain = p.Ismain,
                    SearchTypes = p.SearchTypes.Select(s => new SearchTypeLineDTO()
                    {
                        Name= s.Name,
                        Id = s.Id
                    }).ToList()
                })
                .GetPagedAsync(pagedRequest);

            return result;
        }

        public async Task<ProductDTO?> GetProduct(Guid id)
        {
            var product = await _context.Products
                .Where(p => p.Id == id)
                .Select(p => new ProductDTO
                {
                    Id = p.Id,
                    Description = p.Description,
                    Name = p.Name,
                    Specifications = p.Specifications,
                    CoverImageId = p.CoverImageId,
                    ProductId = p.ProductId,
                    IsMain = p.Ismain,
                    ImageIds = p.Images.Select(i => i.Id).ToList(),
                    SearchTypes = p.SearchTypes.Select(s => new SearchTypeLineDTO
                    {
                        Name = s.Name,
                        Id = s.Id
                    }).ToList(),
                    Products = p.Products.Select(pl => new ProductDTO()
                    {
                        Id = pl.Id,
                        Description = pl.Description,
                        Name = pl.Name,
                        Specifications = pl.Specifications,
                        CoverImageId = pl.CoverImageId
                    }).ToList(),
                })
                .FirstOrDefaultAsync();

            return product;
        }

        public async Task<UpdateResult> UpdateProduct(Guid id, ProductUpdateDTO productDTO)
        {
            var product = await _context.Products
                .FirstOrDefaultAsync(p => p.Id .Equals(id));

            if(product == null)
            {
                return new UpdateResult()
                {
                    Errors = new Dictionary<string, string>()
                    {
                        { "get", "Product not found." }
                    }
                };
            }

            var searchTypes = await _context.SearchTypesLine
                .Where(s => productDTO.SearchTypes.Contains(s.Id))
                .ToListAsync();

            product.Description = productDTO.Description;
            product.Name = productDTO.Name;
            product.Specifications = productDTO.Specifications;
            product.SearchTypes.Clear();
            product.SearchTypes = searchTypes;

            _context.Products.Update(product);
            await _context.SaveChangesAsync();
            
            return new UpdateResult()
            {
                Result = product
            };
        }

        public async Task<Product> CreateProduct(ProductUpdateDTO productDTO)
        {
            var searchTypes = await _context.SearchTypesLine
                .Where(s => productDTO.SearchTypes.Contains(s.Id))
                .ToListAsync();

            var product = new Product()
            {
                Description = productDTO.Description,
                Name = productDTO.Name,
                Specifications = productDTO.Specifications,
                Ismain = productDTO.ProductId == null,
                ProductId = productDTO.ProductId,
                SearchTypes = searchTypes
            };

            productDTO.Id = product.Id;

            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return product;
        }

        public async Task<UpdateResult> DeleteProduct(Guid id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return new UpdateResult()
                {
                    Result = false,
                    Errors = new Dictionary<string, string>()
                    {
                        { "find", "Product not found." }
                    }
                };
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return new UpdateResult()
            {
                Result = true
            };
        }

        public async Task<UpdateResult> CreateProductIamge([FromRoute] Guid id, [FromForm] ImageUpdateDTO imageDTO)
        {
            var product = await _context.Products
                .Include(p => p.CoverImage)
                .Include(p => p.Images)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (product == null)
            {
                return new UpdateResult()
                {
                    Errors = new Dictionary<string, string>()
                    {
                        { "get", "Product not found." }
                    }
                };
            }

            var image = ImageHelper.ResizeImage(imageDTO);

            _context.Images.Add(image);
            await _context.SaveChangesAsync();

            if (imageDTO.IsCoverImage)
            {
                product.CoverImage = image;
            }
            else
            {
                product.Images.Add(image);
            }

            _context.Products.Update(product);
            await _context.SaveChangesAsync();

            return new UpdateResult(image);
        }

    }
}
