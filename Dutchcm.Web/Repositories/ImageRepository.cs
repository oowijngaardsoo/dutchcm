﻿using Dutchcm.Web.Data;
using Dutchcm.Web.Models;
using Dutchcm.Web.Models.Image;
using Dutchcm.Web.Repositories.Implementations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Dutchcm.Web.Repositories
{
    public class ImageRepository : IRepository
    {
        public readonly ApplicationDbContext _context;

        public ImageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ImageDTO?> GetImage(Guid id)
        {
            var image = await _context.Images
                .Select(i => new ImageDTO()
                {
                    Data = i.ImageData,
                    ImageTitle = i.ImageTitle,
                    Id = i.Id,
                    Height = i.Height,
                    Width = i.Width,
                }
                )
                .FirstOrDefaultAsync(i => i.Id == id);

            return image;
        }

        public async Task<UpdateResult> DeleteImage(Guid id)
        {
            var image = await _context.Images
                .FirstOrDefaultAsync(i => i.Id == id);

            if (image == null)
            {
                return new UpdateResult()
                {
                    Errors = new Dictionary<string, string>
                    {
                        { "get", "Image not found." }
                    }
                };
            }

            _context.Images.Remove(image);
            await _context.SaveChangesAsync();

            return new UpdateResult(image);
        }
    }
}
