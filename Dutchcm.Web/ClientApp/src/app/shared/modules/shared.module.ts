import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ImageContainerComponent } from '../components/image-container/image-container.component';
import { MenuItemComponent } from '../components/menu-item/menu-item.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { PaginatorComponent } from '../components/paginator/paginator.component';
import { DefaultModule } from './default.module';

@NgModule({
  imports: [
    CommonModule,
    DefaultModule,
    RouterModule
  ],
  declarations: [
    PaginatorComponent,
    ImageContainerComponent,
    NotFoundComponent,
    MenuItemComponent
  ],
  exports: [
    DefaultModule,
    CommonModule,
    PaginatorComponent,
    ImageContainerComponent,
    NotFoundComponent,
    MenuItemComponent,
    RouterModule
  ]
})

export class SharedModule { }
