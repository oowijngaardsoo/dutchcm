import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';

const modules = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule
]

@NgModule({
  imports: modules,
  exports: modules
})

export class AdminDefaultMaterialsModule { }
