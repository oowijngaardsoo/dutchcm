import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminDefaultMaterialsModule } from './admin-default-materials.module';
import { DefaultModule } from './default.module';


const modules = [
  AdminDefaultMaterialsModule,
  DefaultModule,
  ReactiveFormsModule,
]

@NgModule({
  imports: modules,
  exports: modules
})

export class AdminDefaultModule { }
