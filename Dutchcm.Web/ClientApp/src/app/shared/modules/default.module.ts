import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultMaterialsModule } from './default-materials.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

const modules = [
  CommonModule,
  DefaultMaterialsModule,
  HttpClientModule,
  FormsModule
]

@NgModule({
  imports: modules,
  exports: modules
})

export class DefaultModule { }
