import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PagedResults } from 'src/app/web/shared/models/paged-result';
import { PageIndex } from '../../models/page-index';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
  @Input() pageResults!: PagedResults<any>
  @Output() getPage: EventEmitter<number> = new EventEmitter<number>();

  pages: number[] = [];

  ngOnInit(): void {
    const currentPage = this.pageResults.currentPage;
    if(currentPage > 1){
      for(let i = currentPage - 1; i >= (currentPage > 3 ? currentPage - 3 : 1); i--){
        this.pages.push(i);
      }
    }

    this.pages.push(currentPage);

    if(currentPage <= this.pageResults.pageCount){
      for(let i = currentPage + 1; i <= (this.pageResults.pageCount > 3 ? currentPage + 3 : this.pageResults.pageCount); i++){
        this.pages.push(i);
      }
    }

    this.pages = this.pages.sort();
  }

  onClickEvent = (pageNumber: number) => {
    if(pageNumber != this.pageResults.currentPage)
    this.getPage.emit(pageNumber);
    window.scrollTo(0, 0);
  }

}
