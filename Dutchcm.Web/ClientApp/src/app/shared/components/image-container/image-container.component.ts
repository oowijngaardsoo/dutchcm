import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageCacheData } from '../../models/Image-data';
import { ImageService } from '../../services/image.service';

@Component({
  selector: 'app-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss']
})
export class ImageContainerComponent implements OnInit, OnDestroy, OnChanges {
  @Input() height: string = '100%';
  @Input() width: string = '100%';
  @Input() id!: string|null;
  @Input() fit: 'cover' | 'contain' = 'cover';

  loaded: boolean = false;
  style: string;
  imageStyle: string;
  src: any;
  bg: any;
  constructor(private service: ImageService, private sanitizer: DomSanitizer){
    this.style = `height: ${this.height}; width: ${this.width}`;
    this.imageStyle = `height: ${this.height}; width: ${this.width}`;
    this.src = null;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loaded = false;
    if(this.id != null)
    this.service.getImage(this.id??"").subscribe((image: ImageCacheData) => {
      const src = `data:${image.imageTitle};base64,` + image.data;
      this.src = this.sanitizer.bypassSecurityTrustResourceUrl(src);
      
      this.imageStyle = `
        display: block;
        width: ${this.width};
        height: ${this.height};
        background-image: url(${src});
        background-size: ${this.fit};
        background-position: left center;
      `;

      this.loaded = true;
    });
  }

  ngOnInit(): void {
    this.style = `height: ${this.height}; width: ${this.width}`;
    if(this.id != null)
    this.service.getImage(this.id??"").subscribe((image: ImageCacheData) => {
      const src = `data:${image.imageTitle};base64,` + image.data;
      this.src = this.sanitizer.bypassSecurityTrustResourceUrl(src);
      
      this.imageStyle = `
        display: block;
        width: ${this.width};
        height: ${this.height};
        background-image: url(${src});
        background-size: ${this.fit};
        background-position: left center;
      `;

      this.loaded = true;
    });
  }

  ngOnDestroy(): void {
    this.loaded = false;
    this.src = null;
  }

}
