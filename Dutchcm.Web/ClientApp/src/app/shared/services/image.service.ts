import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ImageCacheData } from '../models/Image-data';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/images`;
  private imageDataCache: ImageCacheData[] = [];

  constructor(private http: HttpClient) { }

  getImage = (id: string): Observable<ImageCacheData> => {
    var filter = this.imageDataCache.filter(i => i.id == id);
    // image in cache
    if (filter.length > 0){
      return of(filter[0]);
    }

    // image in local storage
    const item = localStorage.getItem(id);
    if(item != undefined){
      try{
        const parsedObject = JSON.parse(item) as ImageCacheData;
        this.imageDataCache.push(parsedObject);
  
        return of(parsedObject);
      }
      catch{
        localStorage.clear();
      }
    }

    // get image from database
    return this.http.get<ImageCacheData>(`${this.url}/${id}`).pipe(map((i: ImageCacheData) => {
      this.imageDataCache.push(i);
      localStorage.setItem(i.id, JSON.stringify(i))
      return i;
    }))
  }

  public deleteImage = (id: string) => {
    return this.http.delete(`${this.url}/${id}`);
  }
}
