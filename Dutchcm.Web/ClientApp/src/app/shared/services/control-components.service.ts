import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Column {
  amount: number;
  width: number|null;
}

@Injectable({
  providedIn: 'root'
})

export class ControlComponentsService {
  innerWidthSub: BehaviorSubject<number> = new BehaviorSubject<number>(window.innerWidth);
  innerHeightSub: BehaviorSubject<number> = new BehaviorSubject<number>(window.innerHeight);
  columnsSub: BehaviorSubject<number>;
  private columns: BehaviorSubject<Column[]> = new BehaviorSubject<Column[]>([
    { amount: 1, width: 700 },
    { amount: 2, width: 1300 },
    { amount: 3, width: 2100 },
    { amount: 4, width: null }
  ]);

  constructor(){
    let columns = 4;
    this.columnsSub = new BehaviorSubject<number>(columns);
  }

  setWidth = (width: number): void =>  {
    for(let c of this.columns.value){
      if(c.width == null){
        this.columnsSub.next(c.amount);
        break;
      }
      if(width < c.width){
        this.columnsSub.next(c.amount);
        break;
      }
    }
    this.innerWidthSub.next(width);
  }

  setHeight = (height: number): void => {
    this.innerHeightSub.next(height);
  }

  setColumns = (columns: Column[]) => {
    this.columns.next(columns);
    this.setWidth(window.innerWidth);
    this.setHeight(window.innerHeight);
  }
}
