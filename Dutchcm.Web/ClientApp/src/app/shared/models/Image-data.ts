export interface ImageCacheData {
        id: string;
        data: string;
        imageTitle: string
}
