export interface MenuItem {
        name: string;
        url: string|null;
        exact: boolean,
        items: MenuItem[]|null
}
