export interface PageIndex{
        page: number;
        currentPage: boolean;
}