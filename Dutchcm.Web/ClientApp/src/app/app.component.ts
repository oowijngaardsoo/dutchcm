import { Component, HostListener } from '@angular/core';
import { ControlComponentsService } from './shared/services/control-components.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  @HostListener('window:resize', (['$event']))
  
  SetWindow(event: ResizeObserverEntry) {
    this.ccs.setHeight(window.innerHeight);
    this.ccs.setWidth(window.innerWidth);
  }

  constructor(private ccs:  ControlComponentsService){
    this.ccs.setHeight(window.innerHeight);
    this.ccs.setWidth(window.innerWidth);
  }

  title = 'app';
}
