import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { SearchTypesService } from '../services/search-types.service';

@Injectable({
  providedIn: 'root'
})
export class GetSearchTypeResolver implements Resolve<any> {

  constructor(private service: SearchTypesService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.service.getSearchType(route.params.id);
  }
}
