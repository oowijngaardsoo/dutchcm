import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Project } from '../models/project';
import { ProjectsService } from '../services/projects.service';

@Injectable({
  providedIn: 'root'
})
export class GetProjectsResolver implements Resolve<Project[]> {

  constructor(private service: ProjectsService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project[]> {
    return this.service.getProjects(route.queryParams.searchString);
  }
}
