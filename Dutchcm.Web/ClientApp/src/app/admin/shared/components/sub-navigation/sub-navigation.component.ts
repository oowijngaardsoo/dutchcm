import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

export interface SubNavigationItem<Tkey> {
  id: Tkey,
  title: string|null,
  imageId: string|null
}

@Component({
  selector: 'app-sub-navigation',
  templateUrl: './sub-navigation.component.html',
  styleUrls: ['./sub-navigation.component.scss']
})
export class SubNavigationComponent {
  @Input() url!: 'projecten'|'producten'|'zoektermen';
  @Input() items!: SubNavigationItem<string>[];
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  timer: any;
  searchString: string = '';
  searchForm!: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private fb: FormBuilder){
    this.route.queryParams.subscribe(q => {
      this.searchForm = this.fb.group({
        search: new FormControl(q.searchString)
      });
    });
  }

  onClick = (id: string) => {
    this.router.navigate([`admin/${this.url}/${id}`], { queryParams: { searchString: this.searchString } });
  }

  onCreate = () => {
    this.router.navigate([`admin/${this.url}/add`],  { queryParams: { searchString: this.searchString } });
  }

  onSearch = (search: string) => {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.search.emit(search);
      this.searchString = search;
      this.router.navigate([], { queryParams: { searchString: search } })
    }, 500)
  }
}
