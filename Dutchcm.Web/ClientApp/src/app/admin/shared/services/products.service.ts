import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/products`;
  
  constructor(private http: HttpClient) { }

  public getProducts = (searchString: string = ''): Observable<any[]> => {
    return this.http.get<any[]>(`${this.url}/List?SearchString=${searchString}&cache=no`);
  }

  public getProduct = (id: string): Observable<any> => {
    return this.http.get<any>(`${this.url}/${id}?cache=no`);
  }

  public addProduct = (data: {}) => {
    return this.http.post(`${this.url}?cache=no`, data);
  }

  public addProductImge = (id: string, data: FormData) => {
    return this.http.post(`${this.url}/${id}/image?cache=no`, data);
  }


  public updateProduct = (id: string, data: {}) => {
    return this.http.put(`${this.url}/${id}?cache=no`, data);
  }
}
