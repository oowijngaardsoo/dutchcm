import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchTypesService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/searchtypes`;
  
  constructor(private http: HttpClient) { }

  public getSearchTypes = (searchString: string = ''): Observable<any[]> => {
    return this.http.get<any[]>(`${this.url}/list?SearchString=${searchString}&cache=no`);
  }

  public getSearchType = (id: string): Observable<any> => {
    return this.http.get<any>(`${this.url}/${id}?cache=no`);
  }

  public addSearchType = (data: {}) => {
    return this.http.post(`${this.url}?cache=no`, data);
  }

  public updateSearchType = (id: string, data: {}) => {
    return this.http.put(`${this.url}/${id}?cache=no`, data);
  }

  public deleteSearchType = (id: string) => {
    return this.http.delete(`${this.url}/${id}?cache=no`);
  }

  public getSearchTypeLine = (id: string): Observable<any> => {
    return this.http.get<any>(`${this.url}/line/${id}?cache=no`);
  }

  public addSearchTypeLine = (id: string, data: {}) => {
    return this.http.post(`${this.url}/line/${id}?cache=no`, data);
  }

  public updateSearchTypeLine = (id: string, data: {}) => {
    return this.http.put(`${this.url}/line/${id}?cache=no`, data);
  }

  public deleteSearchTypeLine = (id: string) => {
    return this.http.delete(`${this.url}/line/${id}?cache=no`);
  }

  public getSearchTypesSelect = (ids: string): Observable<any[]> => {
    const array = ids?.length > 0 ? ids.split(",") : null

    let data = {
      ids: [] as string[]
    };

    if(array != null && array?.length > 0){
      data = {
        ids: array
      }
    }
    return this.http.post<any[]>(`${this.url}/select?cache=no`, data);
  }
}
