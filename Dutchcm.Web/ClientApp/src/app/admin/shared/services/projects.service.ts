import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Project } from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/projects`;
  
  constructor(private http: HttpClient) { }

  public getProjects = (searchString: string = ''): Observable<Project[]> => {
    return this.http.get<Project[]>(`${this.url}/list?SearchString=${searchString}&cache=no`);
  }

  public getProject = (id: string): Observable<Project> => {
    return this.http.get<Project>(`${this.url}/${id}?cache=no`);
  }

  public addProject = (data: {}) => {
    return this.http.post(`${this.url}?cache=no`, data);
  }

  public addProjectImge = (id: string, data: FormData) => {
    return this.http.post(`${this.url}/${id}/image?cache=no`, data);
  }


  public updateProject = (id: string, data: {}) => {
    return this.http.put(`${this.url}/${id}?cache=no`, data);
  }
}
