export interface Project {
        id: string;
        coverImageId: string|null;
        architect: string|null;
        assertDate: string;
        client: string|null;
        contractor: string|null;
        description: string;
        imageIds: string[];
        name: string;
}