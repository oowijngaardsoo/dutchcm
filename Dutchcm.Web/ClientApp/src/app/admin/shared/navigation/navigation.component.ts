import { Component } from '@angular/core';
import { MenuItem } from 'src/app/shared/models/menu-item';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  items: MenuItem[];
  loading: boolean = false;

  constructor(private loadingService: LoadingService) { 
    this.items = [
      { url: '/admin', name: 'Dashboard', exact: true, items: null },
      { url: 'projecten', name: 'Projecten', exact: false, items: null },
      { url: 'producten', name: 'Producten', exact: false, items: null },
      { url: 'zoektermen', name: 'Zoektermen', exact: false, items: null },
      { url: '/', name: 'Website', exact: true, items: null },
      { url: '/admin/login', name: 'Uitloggen', exact: true, items: null }
    ];
  }

  ngOnInit(): void {
    this.loadingService.loadingSub.subscribe(loading => {
      this.loading = loading;
    });
  }
}
