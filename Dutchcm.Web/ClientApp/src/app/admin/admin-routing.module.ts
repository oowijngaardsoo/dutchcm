import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { UpdateProductComponent } from './pages/products/form/update/update.component';
import { GetProductInfoComponent } from './pages/products/get-info/get-info.component';
import { ProductsComponent } from './pages/products/products.component';
import { UpdateProjectComponent } from './pages/projects/forms/update/update.component';
import { GetProjectInfoComponent } from './pages/projects/get-info/get-info.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { UpdateSearchTypeComponent } from './pages/search-types/form/update/update.component';
import { GetSearchTypeInfoComponent } from './pages/search-types/get-info/get-info.component';
import { UpdateSearchTypeLineComponent } from './pages/search-types/line/form/update/update.component';
import { SearchTypesComponent } from './pages/search-types/search-types.component';
import { AuthLayout } from './shared/layouts/auth/auth.component';
import { AdminDefaultLayout } from './shared/layouts/default/default.component';
import { GetProductResolver } from './shared/resolvers/get-product.resolver';
import { GetProductsResolver } from './shared/resolvers/get-products.resolve';
import { GetProjectResolver } from './shared/resolvers/get-project.resolver';
import { GetProjectsResolver } from './shared/resolvers/get-projects.resolver';
import { GetSearchTypeLineResolver } from './shared/resolvers/get-search-type-line.resolver';
import { GetSearchTypeResolver } from './shared/resolvers/get-search-type.resolver';
import { GetSearchTypesResolver } from './shared/resolvers/get-search-types.resolver';

const routes: Routes = [
  { path: '', component: AdminDefaultLayout, children: 
    [
      { path: '', component: DashboardComponent },
      { path: 'producten', component: ProductsComponent, runGuardsAndResolvers: 'always', resolve: {products: GetProductsResolver}, children: [
        { path: '', pathMatch: 'full', redirectTo: 'add' },
        { path: 'add', component: UpdateProductComponent },
        { path: ':id', component: GetProductInfoComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {product: GetProductResolver} },
        { path: ':parentid/add', component: UpdateProductComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {project: GetProductResolver} },
        { path: ':id/wijzigen', component: UpdateProductComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {product: GetProductResolver} },
        { path: ':parentid/:id', component: GetProductInfoComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {product: GetProductResolver} },
        { path: ':parentid/:id/wijzigen', component: UpdateProductComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {product: GetProductResolver} },
       
      ] },
      { path: 'projecten', component: ProjectsComponent, runGuardsAndResolvers: 'always', resolve: {projects: GetProjectsResolver}, children: [
        { path: '', pathMatch: 'full', redirectTo: 'add' },
        { path: 'add', component: UpdateProjectComponent },
        { path: ':id', component: GetProjectInfoComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {project: GetProjectResolver} },
        { path: ':id/wijzigen', component: UpdateProjectComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {project: GetProjectResolver} }
      ]  },
      { path: 'zoektermen', component: SearchTypesComponent, runGuardsAndResolvers: 'always', resolve: {searchTypes: GetSearchTypesResolver}, children: [
        { path: '', pathMatch: 'full', redirectTo: 'add' },
        { path: 'add', component: UpdateSearchTypeComponent },
        { path: ':id', component: GetSearchTypeInfoComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {searchType: GetSearchTypeResolver} },
        { path: ':id/add', component: UpdateSearchTypeLineComponent },
        { path: ':parentid/:id/wijzigen', component: UpdateSearchTypeLineComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {searchTypeLine: GetSearchTypeLineResolver} },
        { path: ':id/wijzigen', component: UpdateSearchTypeComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: {searchType: GetSearchTypeResolver} },
      ] }
    ] 
  },
  { path: '', component: AuthLayout, children: 
    [
      { path: 'login', component: LoginComponent }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
