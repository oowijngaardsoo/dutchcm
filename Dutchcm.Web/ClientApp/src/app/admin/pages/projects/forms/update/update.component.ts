import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Project } from 'src/app/admin/shared/models/project';
import { ProjectsService } from 'src/app/admin/shared/services/projects.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateProjectComponent {
  project!: Project;
  form!: FormGroup;
  searchTypes: any;
  loaded: boolean = false;

  constructor(private fb: FormBuilder, private service: ProjectsService, private router: Router, private route: ActivatedRoute){
    this.route.params.subscribe(ps => {
      if(ps.id != undefined){
        this.service.getProject(ps.id).subscribe((project: Project) => {
          this.project = project;
          this.setForm(project);
        });
      }
      else{
        this.setForm(null); 
      }
      
    });
  }

  onSubmit = () => {
    const values = this.form.value;
    let date = moment(this.form.value.assertDate).format('yyyy-MM-DD');

    const data = {
      id: this.project?.id??null,
      name: values.name,
      description: values.description,
      contractor: values.contractor,
      architect: values.architect,
      client: values.client,
      assertDate: date
    };

    if(this.project == null){
      this.service.addProject(data).subscribe((project: any) => {
        this.router.navigate([`admin/projecten/${project.id}`]);
      });
    }
    else{
      this.service.updateProject(this.project.id, data).subscribe((project: any) => {
        this.router.navigate([`admin/projecten/${project.id}`]);
      });
    }
    
  }

  private setForm(project: Project|undefined|null) {
    this.form = this.fb.group({
      id: new FormControl(),
      name: new FormControl(project?.name, [Validators.required, Validators.maxLength(150)]),
      description: new FormControl(project?.description, [Validators.required]),
      assertDate: new FormControl(project?.assertDate != null ? moment(new Date(project.assertDate)) : moment(new Date()), [Validators.required]),
      contractor: new FormControl(project?.contractor, [Validators.maxLength(150)]),
      architect: new FormControl(project?.architect, [Validators.maxLength(150)]),
      client: new FormControl(project?.client, [Validators.maxLength(150)]),
    });
    this.loaded = true;
  }
}
