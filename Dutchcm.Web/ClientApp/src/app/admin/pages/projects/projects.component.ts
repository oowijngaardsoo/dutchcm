import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubNavigationItem } from '../../shared/components/sub-navigation/sub-navigation.component';
import { Project } from '../../shared/models/project';
import { ProjectsService } from '../../shared/services/projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {
  items: SubNavigationItem<string>[] = [];
  loaded: boolean = true;

  constructor(private service: ProjectsService, private route: ActivatedRoute){
    this.route.data.subscribe(data => {
      this.items = [];
      const results = data.projects as Project[];

      results.forEach(p => {
        this.items.push({
          title: p.name,
          id: p.id,
          imageId: p.coverImageId
        });
      });

      this.loaded = true;
    });
  }
}
