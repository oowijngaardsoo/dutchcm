import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/admin/shared/models/project';
import { ProjectsService } from 'src/app/admin/shared/services/projects.service';
import { Column, ControlComponentsService } from 'src/app/shared/services/control-components.service';
import { ImageService } from 'src/app/shared/services/image.service';

@Component({
  selector: 'app-get-info',
  templateUrl: './get-info.component.html',
  styleUrls: ['./get-info.component.scss']
})
export class GetProjectInfoComponent {
  project!: Project;
  loaded: boolean = false;
  imageForm!: FormGroup;
  file!: any;
  file2!: any;
  columns!: number;

  constructor(private service: ProjectsService, private route: ActivatedRoute, private fb: FormBuilder, private router: Router, private ccs: ControlComponentsService, private imageService: ImageService){
    this.loaded = false;

    this.route.data.subscribe((data) => {
      this.project = data.project;
      this.imageForm = this.fb.group({
        file: new FormControl(null, [Validators.required])
      })

      this.loaded = true;
    });

    const columns: Column[] = [
      { amount: 1, width: 900 },
      { amount: 2, width: 1300 },
      { amount: 3, width: 1700 },
      { amount: 4, width: 2000 }
    ]
    ccs.setColumns(columns)
    ccs.columnsSub.subscribe(columns => {
      this.columns = columns;
    });
    
  }

  onCoverImageChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.file = file;
    }
  }

  onImageChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.file2 = file;

      this.onImageSubmit(false);
    }
  }

  onImageSubmit = (coverImage: boolean = true) => {
    const formdata = new FormData();
    formdata.append('File', coverImage ? this.file : this.file2);
    formdata.append('IsCoverImage', `${coverImage}`);
  
    this.service.addProjectImge(this.project.id, formdata).subscribe((image: any) => {
      this.route.queryParams.subscribe(q => {
        this.router.navigate([],
          { queryParams: { searchString: q?.searchString, imageId: image.id } }
        );
      });
    });
  }

  onDeleteImage = (id: string) => {
    const confirm = window.confirm('weet u het zeker?');

    if(confirm)
    this.imageService.deleteImage(id).subscribe(() => {
      this.route.queryParams.subscribe((q) => {
        this.router.navigate([],
        {
          queryParams: { imageId: id+'deleted', searchString: q.searchString }
        });
      });
    });
  }
}
