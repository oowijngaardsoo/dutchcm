import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchTypesService } from 'src/app/admin/shared/services/search-types.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateSearchTypeLineComponent {
  form!: FormGroup;
  searchTypeLine: any;
  loaded: boolean = false;

  constructor(private fb: FormBuilder, private service: SearchTypesService, private router: Router, private route: ActivatedRoute){
    this.route.data.subscribe(data => {
      if(data.searchTypeLine != undefined){
        this.searchTypeLine = data.searchTypeLine;
        this.setForm(data.searchTypeLine);
      }
      else{
        this.setForm(null); 
      }      
    });
  }

  onSubmit = () => {
    const values = this.form.value;

    const data = {
      id: this.searchTypeLine?.id??null,
      name: values.name
    };

    if(this.searchTypeLine == null){
    }
    else{
      this.service.updateSearchTypeLine(this.searchTypeLine.id, data).subscribe((project: any) => {
        this.router.navigate(['../../'], { relativeTo: this.route });
      });
    }
  }

  private setForm(searchTypeLine: any|undefined|null) {
    this.form = this.fb.group({
      id: new FormControl(searchTypeLine?.id),
      name: new FormControl(searchTypeLine?.name, [Validators.required, Validators.maxLength(150)])
    });
    this.loaded = true;
  }
}
