import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubNavigationItem } from '../../shared/components/sub-navigation/sub-navigation.component';

@Component({
  selector: 'app-search-types',
  templateUrl: './search-types.component.html',
  styleUrls: ['./search-types.component.scss']
})
export class SearchTypesComponent {
  items: SubNavigationItem<string>[] = [];
  loaded: boolean = true;

  constructor(private route: ActivatedRoute){
    this.route.data.subscribe(data => {
      this.items = [];
      const results = data.searchTypes as any[];

      results.forEach(p => {
        this.items.push({
          title: p.name,
          id: p.id,
          imageId: p.coverImageId
        });
      });

      this.loaded = true;
    });
  }
}
