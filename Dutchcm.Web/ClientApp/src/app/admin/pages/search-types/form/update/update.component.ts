import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { SearchTypesService } from 'src/app/admin/shared/services/search-types.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateSearchTypeComponent {
  searchType!: any;
  form!: FormGroup;
  loaded: boolean = false;

  constructor(private fb: FormBuilder, private service: SearchTypesService, private router: Router, private route: ActivatedRoute){
    this.route.data.subscribe(data => {
      if(data.searchType != undefined){
        this.searchType = data.searchType;
        this.setForm(data.searchType);
      }
      else{
        this.setForm(null); 
      }      
    });
  }

  onSubmit = () => {
    const values = this.form.value;

    const data = {
      id: this.searchType?.id??null,
      name: values.name
    };

    if(this.searchType == null){
      this.service.addSearchType(data).subscribe((project: any) => {
        this.router.navigate([`admin/zoektermen/${project.id}`]);
      });
    }
    else{
      this.service.updateSearchType(this.searchType.id, data).subscribe((project: any) => {
        this.router.navigate([`admin/zoektermen/${project.id}`]);
      });
    }
    
  }

  private setForm(searchType: any|undefined|null) {
    this.form = this.fb.group({
      id: new FormControl(),
      name: new FormControl(searchType?.name, [Validators.required, Validators.maxLength(150)])
    });
    this.loaded = true;
  }
}
