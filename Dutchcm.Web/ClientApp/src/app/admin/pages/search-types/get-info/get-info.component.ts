import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchTypesService } from 'src/app/admin/shared/services/search-types.service';

@Component({
  selector: 'app-get-info',
  templateUrl: './get-info.component.html',
  styleUrls: ['./get-info.component.scss']
})
export class GetSearchTypeInfoComponent {
  searchType!: any;
  loaded: boolean = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private router: Router, private service: SearchTypesService){
    this.loaded = false;
    this.form = this.fb.group({
      name: new FormControl('', [Validators.required])
    });
    this.route.data.subscribe((data) => {
      this.searchType = data.searchType;

      this.loaded = true;
    });
  }


  onSubmit = () => {
    const data = {
      name: this.form.value.name
    };

    this.service.addSearchTypeLine(this.searchType.id, data).subscribe((result: any) => {
      this.route.queryParams.subscribe(q => {
        this.router.navigate([], {
          queryParams: { searchString: q.searchString, lineid: result.id }
        })
      });
      
    });
  }

  onDeleteSearchType = (id: string) => {
    const comfirm = window.confirm('Weet u het zeker?');

    if(comfirm)
    this.service.deleteSearchType(id).subscribe(result => {
      this.route.queryParams.subscribe(q => {
        this.router.navigate(['../'], {relativeTo: this.route, queryParams:  { searchString: q.searchString, id:result }});
      });
    });
  }

  onDeleteSearchTypeLine = (id: string) => {
    const comfirm = window.confirm('Weet u het zeker?');
    console.log(id)
    if(comfirm)
    this.service.deleteSearchTypeLine(id).subscribe(result => {
      this.route.queryParams.subscribe(q => {
        this.router.navigate([], { queryParams: { searchString: q.searchString, id:result }});
      });
    });
  }

}
