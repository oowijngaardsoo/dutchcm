import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from 'src/app/admin/shared/services/products.service';
import { SearchTypesService } from 'src/app/admin/shared/services/search-types.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateProductComponent {
  product!: any;
  form!: FormGroup;
  searchTypes: any;
  loaded: boolean = false;

  constructor(private fb: FormBuilder, private service: ProductsService, private router: Router, private route: ActivatedRoute, private sts: SearchTypesService){
    this.sts.getSearchTypesSelect("").subscribe(st => {
      this.searchTypes = st;
      this.route.params.subscribe(ps => {
        if(ps.id != undefined){
          this.service.getProduct(ps.id).subscribe((product: any) => {
            this.product = product;
            this.setForm(product);
          });
        }
        else if (ps.parentid != undefined){    
          this.setForm({
            productId: ps.parentid
          }); 
        }
        else{
          this.setForm(null); 
        }
        
      });
    
    });
  }

  onSubmit = () => {
    const values = this.form.value;
    const data = {
      id: this.product?.id??null,
      productId: values.productId,
      name: values.name,
      description: values.description,
      specifications: values.specifications,
      searchTypes: values.searchTypes
    };

    if(this.product == null){
      this.service.addProduct(data).subscribe((product: any) => {
        const id = product.id != undefined ? product.id 
          : this.product.id??this.product.productId
          console.log(product.id)
        this.router.navigate(['admin', 'producten', id]);
      });
    }
    else{
      this.service.updateProduct(this.product?.id, data).subscribe((product: any) => {
        this.router.navigate([`admin/producten/${this.product.id??product.id}`]);
      });
    }
    
  }

  private setForm(product: any|undefined|null) {
    var ar = product?.searchTypes as any[];

    this.form = this.fb.group({
      id: new FormControl(product?.id),
      productId: new FormControl(product?.productId),
      name: new FormControl(product?.name, [Validators.required, Validators.maxLength(150)]),
      description: new FormControl(product?.description, [Validators.required]),
      specifications: new FormControl(product?.specifications, []),
      searchTypes: new FormControl(ar?.map(s => s.id), [Validators.required]),
    });
    this.loaded = true;
  }
}
