import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminDefaultLayout } from './shared/layouts/default/default.component';
import { AdminDefaultModule } from '../shared/modules/admin-default.module';
import { AuthLayout } from './shared/layouts/auth/auth.component';
import { LoginComponent } from './pages/login/login.component';
import { ProductsComponent } from './pages/products/products.component';
import { SharedModule } from '../shared/modules/shared.module';
import { DefaultModule } from '../shared/modules/default.module';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SearchTypesComponent } from './pages/search-types/search-types.component';
import { SubNavigationComponent } from './shared/components/sub-navigation/sub-navigation.component';
import { UpdateProjectComponent } from './pages/projects/forms/update/update.component';
import { LoadingService } from '../shared/services/loading.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ProjectsService } from './shared/services/projects.service';
import { GetProjectInfoComponent } from './pages/projects/get-info/get-info.component';
import { ImageService } from '../shared/services/image.service';
import { GetProjectResolver } from './shared/resolvers/get-project.resolver';
import { GetProjectsResolver } from './shared/resolvers/get-projects.resolver';
import { UpdateProductComponent } from './pages/products/form/update/update.component';
import { GetProductInfoComponent } from './pages/products/get-info/get-info.component';
import { ProductsService } from './shared/services/products.service';
import { GetProductResolver } from './shared/resolvers/get-product.resolver';
import { GetProductsResolver } from './shared/resolvers/get-products.resolve';
import { GetSearchTypesResolver } from './shared/resolvers/get-search-types.resolver';
import { UpdateSearchTypeComponent } from './pages/search-types/form/update/update.component';
import { GetSearchTypeInfoComponent } from './pages/search-types/get-info/get-info.component';
import { SearchTypesService } from './shared/services/search-types.service';
import { GetSearchTypeResolver } from './shared/resolvers/get-search-type.resolver';
import { GetSearchTypeLineResolver } from './shared/resolvers/get-search-type-line.resolver';
import { UpdateSearchTypeLineComponent } from './pages/search-types/line/form/update/update.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@NgModule({
  declarations: [
    AdminDefaultLayout,
    AuthLayout,
    LoginComponent,
    ProductsComponent,
    NavigationComponent,
    ProjectsComponent,
    DashboardComponent,
    SearchTypesComponent,
    SubNavigationComponent,
    UpdateProjectComponent,
    GetProjectInfoComponent,
    UpdateProductComponent,
    GetProductInfoComponent,
    UpdateSearchTypeComponent,
    GetSearchTypeInfoComponent,
    UpdateSearchTypeLineComponent,
  ],
  imports: [
    AdminDefaultModule,
    AdminRoutingModule,
    DefaultModule,
    SharedModule
  ],
  providers: [
    ProjectsService,
    ProductsService,
    LoadingService,
    SearchTypesService,
    ImageService,
    GetProjectResolver,
    GetProjectsResolver,
    GetProductResolver,
    GetProductsResolver,
    GetSearchTypesResolver,
    GetSearchTypeResolver,
    GetSearchTypeLineResolver,
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},

  ]
})
export class AdminModule { }
