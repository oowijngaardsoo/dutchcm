import { Component, Input, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() showService: boolean = true;
  @Input() showContact: boolean = true;
  @Input() showSocial: boolean = true;

  loading: boolean = false;

  constructor(private loadingService: LoadingService) { }

  ngOnInit(): void {
    this.loadingService.loadingSub.subscribe(loading => {
      this.loading = loading;
    });
  }

}
