import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/shared/models/menu-item';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  items: MenuItem[];
  loading: boolean = false;

  constructor(private loadingService: LoadingService) { 
    this.items = [
      { url: '', name: 'Home', exact: true, items: null },
      { url: 'projecten', name: 'Projecten', exact: false, items: null },
      { url: 'producten', name: 'Producten', exact: false, items: null },
      // { url: 'nieuws', name: 'Nieuws', exact: true, items: null },
      { url: 'zakelijk', name: 'Zakelijk', exact: true, items: null },
      { url: 'over-ons', name: 'Over Ons', exact: false, items: [
        { url: 'over-ons/transport-montage', name: 'Transport & Montage', exact: false, items: null },
        { url: 'over-ons/klantenservice', name: 'klantenservice', exact: false, items: null },
        { url: 'over-ons/kwaliteit-garantie', name: 'Kwaliteit & Garantie', exact: false, items: null },
      ] },
      { url: 'contact', name: 'Contact', exact: true, items: null }
    ];
  }

  ngOnInit(): void {
    this.loadingService.loadingSub.subscribe(loading => {
      this.loading = loading;
    });
  }

}
