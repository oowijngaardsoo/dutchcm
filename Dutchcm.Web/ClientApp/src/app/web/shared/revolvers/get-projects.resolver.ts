import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Router,
  ActivatedRoute,
} from '@angular/router';
import { map, Observable } from 'rxjs';
import { PagedResults } from '../models/paged-result';
import { Project } from '../models/project';
import { ProjectsService } from '../services/projects.service';

@Injectable({
  providedIn: 'root'
})
export class GetProjectsResolver implements Resolve<PagedResults<Project>> {
  page: number = 1;
  constructor(private service: ProjectsService, private router: Router, private route: ActivatedRoute){
   
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PagedResults<Project>> {
      const number: number = route.queryParams.page??1
      return this.service.getProjects(number)
      .pipe(map(paged => {
        if(paged.currentPage > paged.pageCount){
          this.router.navigate(['projecten'], {
            relativeTo: this.route,
            queryParams: { page : paged.pageCount }
          })
        }
        return paged;
      }));     
  }
}
