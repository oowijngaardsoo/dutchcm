import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { catchError, map, Observable, of } from 'rxjs';
import { PagedResults } from '../models/paged-result';
import { SearchTypesService } from '../services/search-types.service';

@Injectable({
  providedIn: 'root'
})
export class GetSearchTypesResolver implements Resolve<PagedResults<any>> {

  constructor(private sts: SearchTypesService, private router: Router){
   
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const ids: string = route.queryParams.checked??""
      
      return this.sts.getSearchTypes(ids)
      .pipe(
        map(result => {
          return result;
        }), 
        catchError((error) => {
          this.router.navigate(['producten']);
          console.log(error)
          return [];
        })
      );       
  }
}
