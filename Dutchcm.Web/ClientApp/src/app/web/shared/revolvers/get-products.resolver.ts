import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Router,
  ActivatedRoute,
} from '@angular/router';
import { catchError, map, Observable, of } from 'rxjs';
import { PagedResults } from '../models/paged-result';
import { ProductsService } from '../services/products.service';

@Injectable({
  providedIn: 'root'
})
export class GetProductsResolver implements Resolve<PagedResults<any>> {
  page: number = 1;
  constructor(private service: ProductsService, private router: Router, private route: ActivatedRoute){
   
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PagedResults<any>> {
      const number: number = route.queryParams.page??1
      const ids: string = route.queryParams.checked??null;
      return this.service.getProducts(number, ids)
        .pipe(map(paged => {
          if(paged.currentPage > paged.pageCount){
            this.router.navigate(['producten'], {
              relativeTo: this.route,
              queryParams: { page : paged.pageCount, checked: ids }
            })
          }
          return paged;
        }), catchError((error) => {
          this.router.navigate(['producten'])

          return of();
        }));     
  }
}
