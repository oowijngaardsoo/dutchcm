import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PagedResults } from '../models/paged-result';
import { Project } from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/projects`;
  
  constructor(private http: HttpClient) { }

  public getProjects = (page: number = 1): Observable<PagedResults<Project>> => {
    return this.http.get<PagedResults<Project>>(`${this.url}?amount=10&page=${page}`);
  }
}
