import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PagedResults } from '../models/paged-result';
import { environment } from '../../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/products`;

  constructor(private http: HttpClient) { }

  public getProducts = (page: number = 1, ids: string): Observable<PagedResults<any>> => {
    const array = ids?.length > 0 ? ids?.split(",") : null;
    return this.http.post<PagedResults<any>>(`${this.url}/page`,
    {
      ids: array,
      page: page,
      Amount: 24
    });
  }
}
