import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchTypesService {
  private port: number = environment.port;
  private hostname: string = `https://localhost:${this.port}`;
  private url: string = `${this.hostname}/api/searchtypes`;
  
  constructor(private http: HttpClient) { }

  public getSearchTypes = (ids: string): Observable<any[]> => {
    const array = ids?.length > 0 ? ids.split(",") : null

    let data = {
      ids: [] as string[]
    };

    if(array != null && array?.length > 0){
      data = {
        ids: array
      }
    }
    return this.http.post<any[]>(`${this.url}/products`, data);
  }
}
