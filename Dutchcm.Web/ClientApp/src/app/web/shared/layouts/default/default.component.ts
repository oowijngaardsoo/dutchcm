import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class WebDefaultLayout implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
