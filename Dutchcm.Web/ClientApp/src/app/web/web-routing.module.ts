import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '../shared/components/not-found/not-found.component';
import { BusinessComponent } from './pages/business/business.component';
import { ContactComponent } from './pages/contact/contact.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { NewsComponent } from './pages/news/news.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { WebDefaultLayout } from './shared/layouts/default/default.component';
import { WebProductsLayout } from './shared/layouts/products/products.component';
import { GetProductsResolver } from './shared/revolvers/get-products.resolver';
import { GetProjectsResolver } from './shared/revolvers/get-projects.resolver';
import { GetSearchTypesResolver } from './shared/revolvers/get-search-types.resolver';

const routes: Routes = [
  { path: '', component: WebDefaultLayout, children: 
    [
      { path: '', component: LandingPageComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'nieuws', component: NewsComponent },
    ] 
  },
  { path: '', component: WebProductsLayout, children: [
      { path: 'producten', component: ProductsComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: { products: GetProductsResolver, searchTypes: GetSearchTypesResolver } },
      { path: 'projecten', component: ProjectsComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', resolve: { projects: GetProjectsResolver } },
      { path: 'zakelijk', component: BusinessComponent },
      { path: 'not-found', component: NotFoundComponent }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class WebRoutingModule { }
