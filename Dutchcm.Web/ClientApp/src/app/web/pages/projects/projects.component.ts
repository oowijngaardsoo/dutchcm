import { Component } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { PagedResults } from '../../shared/models/paged-result';
import { Project } from '../../shared/models/project';
import { ProjectsService } from '../../shared/services/projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {
  projectPagedResult!: PagedResults<Project>
  loaded = false;

  constructor(private router: Router, private route: ActivatedRoute){
    this.route.data.subscribe(data => {
      this.projectPagedResult = data["projects"];
      this.loaded = true;
    });
  }

  onPageChange = (pageNumber: number) => {
    this.router.navigate(
      ['/projecten'],
      { queryParams: { page: pageNumber } }
    );
  }
}
