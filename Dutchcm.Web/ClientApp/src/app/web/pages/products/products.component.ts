import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ControlComponentsService } from 'src/app/shared/services/control-components.service';
import { PagedResults } from '../../shared/models/paged-result';
import { ProductsService } from '../../shared/services/products.service';
import { SearchTypesService } from '../../shared/services/search-types.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {
  items: any;
  checkedItems: string[] = [];
  productsPageResults!: PagedResults<any>;
  loaded: boolean = false;
  columns!: number;
  constructor(private router: Router, private route: ActivatedRoute, private ccs: ControlComponentsService){
    ccs.columnsSub.subscribe(columns => {
      this.columns = columns;
    })
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe(q => {
      const checked = q.checked as string|null;
      if(checked != null){
        this.checkedItems = checked?.split(',').filter(r => r.length > 0 || r != null)
      }
      else{
        this.checkedItems = [];
      }
      this.route.data.subscribe(data => {
        this.productsPageResults = data["products"];
        this.items = data['searchTypes']
        this.loaded = true;
      });
    });
  }

  onToggle = (id: string, checked: boolean) => {
    if(checked){
      this.checkedItems.push(id);
    }
    else{
      this.checkedItems = this.checkedItems.filter(c => c != id);
    }

    let ar = this.checkedItems.join(",");
    if(ar.length > 0){
      const isComma = ar.slice(0,1) == ',';
      if(isComma){
        ar = ar.slice(1, ar.length);
      }
    }
   

    this.router.navigate(['/producten'],
    { queryParams: { checked: ar } }
    )
  }
  
  onPageChange = (pageNumber: number) => {
    let ar = this.checkedItems.join(",");
    if(ar.length > 0){
      const isComma = ar.slice(0,1) == ',';
      if(isComma){
        ar = ar.slice(1, ar.length);
      }
    }

    this.router.navigate(
      ['/producten'],
      { queryParams: { page: pageNumber, checked: ar } }
    );
  }
}
