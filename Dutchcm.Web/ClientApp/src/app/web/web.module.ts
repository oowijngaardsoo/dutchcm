import { NgModule } from '@angular/core';
import { WebRoutingModule } from './web-routing.module';
import { WebDefaultLayout } from './shared/layouts/default/default.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { FooterComponent } from './shared/footer/footer.component';

//Materials
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProductsComponent } from './pages/products/products.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NewsComponent } from './pages/news/news.component';
import { BusinessComponent } from './pages/business/business.component';
import { WebProductsLayout } from './shared/layouts/products/products.component';
import { ProjectsService } from './shared/services/projects.service';
import { ImageService } from '../shared/services/image.service';
import { GetProjectsResolver } from './shared/revolvers/get-projects.resolver';
import { LoadingService } from '../shared/services/loading.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpRequestInterceptor } from '../shared/interceptors/http-request.interceptor';
import { SearchTypesService } from './shared/services/search-types.service';
import { ProductsService } from './shared/services/products.service';
import { GetProductsResolver } from './shared/revolvers/get-products.resolver';
import { GetSearchTypesResolver } from './shared/revolvers/get-search-types.resolver';
import { SharedModule } from '../shared/modules/shared.module';

@NgModule({
  declarations: [
    WebDefaultLayout,
    WebProductsLayout,
    NavigationComponent,
    FooterComponent,
    LandingPageComponent,
    ProjectsComponent,
    ProductsComponent,
    ContactComponent,
    NewsComponent,
    BusinessComponent
  ],
  imports: [
    WebRoutingModule,
    SharedModule
  ],
  providers: [
    ProjectsService,
    ProductsService,
    SearchTypesService,
    ImageService,
    LoadingService,
    GetProjectsResolver,
    GetProductsResolver,
    GetSearchTypesResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true
    }
  ]
})
export class WebModule { }
