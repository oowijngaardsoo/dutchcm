const { env } = require('process');

const target = env.ASPNETCORE_HTTPS_PORT ? `https://localhost:${env.ASPNETCORE_HTTPS_PORT}` :
  env.ASPNETCORE_URLS ? env.ASPNETCORE_URLS.split(';')[0] : 'http://localhost:23866';

console.log('target: ', target);

const PROXY_CONFIG = [
  {
    context: [
      "/weatherforecast",
      "/api",
      "/_configuration",
      "/.well-known",
      "/Identity",
      "/connect",
      "/ApplyDatabaseMigrations",
      "/_framework"
   ],
    target: target,
    secure: false,
    headers: {
      Connection: 'Keep-Alive'
    }
  }
]

console.log('PROXY_CONFIG: ', PROXY_CONFIG);

module.exports = PROXY_CONFIG;
