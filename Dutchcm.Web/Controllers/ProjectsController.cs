﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dutchcm.Web.Data;
using Dutchcm.Web.Data.Entities;
using Dutchcm.Web.Models;
using Dutchcm.Web.Helpers;
using Dutchcm.Web.Models.Requests;
using Dutchcm.Web.Models.Image;
using Dutchcm.Web.Models.Project;

namespace Dutchcm.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ProjectsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Projects
        [HttpGet("list")]
        public async Task<ActionResult<Project>> GetProjects([FromQuery] PagedRequest pagedRequest)
        {
            if (_context.Projects == null)
            {
                return NotFound();
            }

            var query = _context.Projects.AsQueryable();

            if (!string.IsNullOrWhiteSpace(pagedRequest.SearchString))
            {
                query = query.Where(p =>
                    (p.Contractor != null && p.Contractor.ToLower().Contains(pagedRequest.SearchString.ToLower()))
                    || (p.Client != null && p.Client.ToLower().Contains(pagedRequest.SearchString.ToLower()))
                    || (p.Architect != null && p.Architect.ToLower().Contains(pagedRequest.SearchString.ToLower()))
                    || p.Name.ToLower().Contains(pagedRequest.SearchString.ToLower())
                );
            }

            var result = await query
                .OrderByDescending(s => s.Created)
                .ToListAsync();

            return Ok(result);
        }


        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<PagedResult<ProjectDTO>>> GetProjectsPaged([FromQuery] PagedRequest pagedRequest)
        {
            if (_context.Projects == null)
            {
                return NotFound();
            }

            var query = _context.Projects
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(pagedRequest.SearchString))
            {
                query = query.Where(p => 
                    (p.Architect != null && p.Architect.StartsWith(pagedRequest.SearchString))
                    || (p.Client != null && p.Client.StartsWith(pagedRequest.SearchString))
                    || (p.Contractor != null && p.Contractor.StartsWith(pagedRequest.SearchString))
                    || p.Name.Contains(pagedRequest.SearchString)
                );
            }

            var result = await query
                .Where(p => p.CoverImageId != null)
                .Select(p => new ProjectDTO
                {
                    Description= p.Description,
                    Name= p.Name,
                    Architect= p.Architect,
                    AssertDate= p.AssertDate,
                    Client= p.Client,
                    Contractor= p.Contractor,
                    CoverImageId= p.CoverImageId,
                    Id= p.Id
                })
                .GetPagedAsync(pagedRequest);

            return Ok(result);
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProject(Guid id)
        {
            if (_context.Projects == null)
            {
                return NotFound();
            }
            var project = await _context.Projects
                    .Select(p => new ProjectDTO
                    {
                        Description = p.Description,
                        Name = p.Name,
                        Architect = p.Architect,
                        AssertDate = p.AssertDate,
                        Client = p.Client,
                        Contractor = p.Contractor,
                        CoverImageId = p.CoverImageId,
                        ImageIds = p.Images.Select(i => i.Id).ToList(),
                        Id = p.Id
                    })
                .FirstOrDefaultAsync(p => p.Id == id);

            if (project == null)
            {
                return NotFound();
            }

            return project;
        }

        // PUT: api/Projects/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProject(Guid id, ProjectUpdateDTO projectDTO)
        {
            if (id != projectDTO.Id)
            {
                return BadRequest();
            }

            var project = await _context.Projects.FindAsync(id);

            if (project == null)
            { 
                return BadRequest();
            }


            try
            {
                var query = _context.Projects
                    .Where(p => p.Id == id)
                    .AsQueryable();

                await query
                    .ExecuteUpdateAsync(p =>
                        p.SetProperty(x => x.AssertDate, DateTime.Parse(projectDTO.AssertDate))
                        .SetProperty(x => x.Architect, projectDTO.Architect)
                        .SetProperty(x => x.Name, projectDTO.Name)
                        .SetProperty(x => x.Description, projectDTO.Description)
                );

                return Ok(projectDTO);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // POST: api/Projects
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Project>> PostProject(ProjectUpdateDTO projectDTO)
        {
            if (_context.Projects == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Projects'  is null.");
            }

            var project = new Project 
            {
                Name = projectDTO.Name,
                Description = projectDTO.Description,
                AssertDate= DateTime.Parse( projectDTO.AssertDate),
                Architect= projectDTO.Architect,
                Client= projectDTO.Client,
                Contractor= projectDTO.Contractor
            };

            var result = _context.Projects.Add(project);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProject", new { id = project.Id }, result.Entity);
        }

        [HttpPost("{id}/image")]
        public async Task<IActionResult> PostProjectIamge([FromRoute] Guid id, [FromForm] ImageUpdateDTO imageDTO)
        {
            var project = await _context.Projects
                .Include(p => p.CoverImage)
                .Include(p => p.Images)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (project == null)
            {
                return BadRequest();
            }
            
            try
            {
                var image = ImageHelper.ResizeImage(imageDTO);

                var result = _context.Images.Add(image);
                await _context.SaveChangesAsync();

                if (imageDTO.IsCoverImage)
                {
                    if(project.CoverImage != null)
                    {
                        _context.Images.Remove(project.CoverImage);
                        await _context.SaveChangesAsync();
                        
                    }

                    project.CoverImage = image;
                }
                else
                {
                    project.Images.Add(image);
                }
                   
                _context.Projects.Update(project);
                await _context.SaveChangesAsync();

                return Ok(result.Entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            
        }


        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(Guid id)
        {
            if (_context.Projects == null)
            {
                return NotFound();
            }
            var project = await _context.Projects.FindAsync(id);
            if (project == null)
            {
                return NotFound();
            }

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProjectExists(Guid id)
        {
            return (_context.Projects?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
