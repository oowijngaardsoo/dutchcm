﻿using Dutchcm.Web.Models.Image;
using Dutchcm.Web.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Dutchcm.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {

        private readonly ImageRepository _repository;

        public ImagesController(ImageRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Images/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ImageDTO>> GetImage(Guid id)
        {
            var result = await _repository.GetImage(id);

            if(result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteImage(Guid id)
        {
            var result = await _repository.DeleteImage(id);

            if (result.IsSuccess)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
