﻿using Microsoft.AspNetCore.Mvc;
using Dutchcm.Web.Data;
using Dutchcm.Web.Data.Entities;
using Dutchcm.Web.Models;
using Dutchcm.Web.Models.Requests;
using Dutchcm.Web.Models.Image;
using Dutchcm.Web.Models.Product;
using Dutchcm.Web.Repositories;

namespace Dutchcm.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductRepository _repository;

        public ProductsController(ProductRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Products
        [HttpGet("List")]
        public async Task<ActionResult<List<ProductDTO>>> GetProducts([FromQuery] SearchRequest searchRequest)
        {
            var result = await _repository.GetProducts(searchRequest);

            return Ok(result);
        }


        [HttpPost("page")]
        public async Task<ActionResult<PagedResult<ProductDTO>>> GetProductsPaged(PagedRequest pagedRequest)
        {
            var result = await _repository.GetProductsPaged(pagedRequest);

            return Ok(result);
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(Guid id)
        {
            var result = await _repository.GetProduct(id);

            if(result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(Guid id, ProductUpdateDTO productDTO)
        {
            var result = await _repository.UpdateProduct(id, productDTO);

            if (!result.IsSuccess)
            {
                return BadRequest(result);
            }

            return Ok(result.Result);
        }

        // POST: api/Products
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(ProductUpdateDTO productDTO)
        {
            var result = await _repository.CreateProduct(productDTO);

            return CreatedAtAction("GetProduct", new { id = result.Id }, result);
        }

        [HttpPost("{id}/image")]
        public async Task<IActionResult> PostProductIamge([FromRoute] Guid id, [FromForm] ImageUpdateDTO imageDTO)
        {
            var result = await _repository.CreateProductIamge(id, imageDTO);

            if(result.IsSuccess ) 
            { 
                return Ok(result);
            }

            return BadRequest(result);
        }


        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            var result = await _repository.DeleteProduct(id);

            if (result.IsSuccess)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
