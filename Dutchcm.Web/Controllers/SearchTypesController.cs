﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dutchcm.Web.Data;
using Dutchcm.Web.Data.Entities;
using Dutchcm.Web.Models;
using Dutchcm.Web.Models.Requests;

namespace Dutchcm.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchTypesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SearchTypesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/SearchTypes
        [HttpPost("products")]
        public async Task<ActionResult<IEnumerable<SearchTypeDTO>>> GetSearchTypes(SearchTypeRequest request)
        {
            if (_context.SearchTypes == null)
            {
                return NotFound();
            }

            var query = _context.SearchTypes.AsQueryable();
            var hasProducts = true;

            if (request.Ids != null && request.Ids.Length > 0) 
            {
                var pIds = await _context.Products
                    .Where(p => p.Ismain && !p.IsDeleted && p.CoverImageId != null)
                    .Where(p => p.SearchTypes.Where(s => request.Ids.Contains(s.Id)).Any())
                    .SelectMany(p => p.SearchTypes.Select(s => s.Id))
                    .Distinct()
                    .ToListAsync();

                if(pIds.Any())
                {
                    query = query.Include(i => i.Lines.Where(l => pIds.Contains(l.Id)))
                        .Where(i => i.Lines.Where(l => pIds.Contains(l.Id)).Any());
                    hasProducts = true;
                }
                else
                {
                    hasProducts = false;
                }
            }
            else if (request.Ids == null || request.Ids.Length == 0)
            {
                var pIds = await _context.Products
                   .Where(p => p.Ismain && !p.IsDeleted && p.CoverImageId != null)
                   .SelectMany(p => p.SearchTypes.Select(s => s.Id))
                   .Distinct()
                   .ToListAsync();

                query = query.Include(i => i.Lines.Where(l => pIds.Contains(l.Id)))
                       .Where(i => i.Lines.Where(l => pIds.Contains(l.Id)).Any());
            }

            if (hasProducts && query.Any())
            {
                var result = await query
                    .ToListAsync();

                return result
                    .Select(s => new SearchTypeDTO()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Lines = s.Lines.Select(l => new SearchTypeLineDTO()
                        {
                            Id = l.Id,
                            Name = l.Name
                        }).ToList()
                    })
                    .ToList();
            }
            else
            {

                var result =  await _context.SearchTypes
                    .Include(s => s.Lines.Where(l => request.Ids.Contains(l.Id)))
                    .Where(s => s.Lines.Where(l => request.Ids.Contains(l.Id)).Any())
                    .ToListAsync();

                return result
                    .Where(s => s.Lines.Count > 0)
                    .Select(s => new SearchTypeDTO()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Lines = s.Lines.Select(l => new SearchTypeLineDTO()
                        {
                            Id = l.Id,
                            Name = l.Name
                        }).ToList()
                    })
                    .ToList();
            }
        }

        // GET: api/SearchTypes/list
        [HttpGet("List")]
        public async Task<ActionResult<List<SearchTypeDTO>>> GetSearchTypeList([FromQuery] PagedRequest pagedRequest)
        {
            if (_context.SearchTypes == null)
            {
                return NotFound();
            }

            var query = _context.SearchTypes
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(pagedRequest.SearchString))
            {
                query = query.Where(s => s.Name.ToLower().Contains(pagedRequest.SearchString.ToLower()));
            }

            var searchType = await query
                .Select(s => new SearchTypeDTO
                {
                    Id = s.Id,
                    Name = s.Name,
                })
                .OrderBy(s => s.Name)
                .ToListAsync();

            if (searchType == null)
            {
                return NotFound();
            }

            return searchType;
        }

        // GET: api/SearchTypes/Select
        [HttpPost("Select")]
        public async Task<ActionResult<List<SearchTypeDTO>>> GetSearchTypeSelect([FromQuery] PagedRequest pagedRequest)
        {
            if (_context.SearchTypes == null)
            {
                return NotFound();
            }

            var query = _context.SearchTypes
                .Include(s => s.Lines)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(pagedRequest.SearchString))
            {
                query = query.Where(s => s.Name.ToLower().Contains(pagedRequest.SearchString.ToLower()));
            }

            var searchType = await query
                .Select(s => new SearchTypeDTO
                {
                    Id = s.Id,
                    Name = s.Name,
                    Lines = s.Lines.Select(l => new SearchTypeLineDTO()
                    {
                        Id = l.Id,
                        Name = l.Name
                    }).ToList()
                })
                .ToListAsync();

            return searchType;
        }

        // GET: api/SearchTypes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SearchTypeDTO>> GetSearchType(Guid id)
        {
          if (_context.SearchTypes == null)
          {
              return NotFound();
          }
            var searchType = await _context.SearchTypes
                .Include(s => s.Lines)
                .Select(s => new SearchTypeDTO()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Lines = s.Lines.Select(l => new SearchTypeLineDTO()
                    {
                        Name= l.Name,
                        Id = l.Id
                    }).ToList()
                })
                .FirstOrDefaultAsync(s => s.Id == id);

            if (searchType == null)
            {
                return NotFound();
            }

            return searchType;
        }

        // GET: api/SearchTypes/5
        [HttpGet("line/{id}")]
        public async Task<ActionResult<SearchTypeLineDTO>> GetSearchLineType(Guid id)
        {
            if (_context.SearchTypes == null)
            {
                return NotFound();
            }
            var searchType = await _context.SearchTypesLine
                .Select(s => new SearchTypeLineDTO()
                {
                    Id = s.Id,
                    Name = s.Name
                })
                .FirstOrDefaultAsync(s => s.Id == id);

            if (searchType == null)
            {
                return NotFound();
            }

            return searchType;
        }

        // PUT: api/SearchTypes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSearchType(Guid id, SearchTypeDTO searchTypeDTO)
        {
            if (id != searchTypeDTO.Id)
            {
                return BadRequest();
            }

            var searchType = await _context.SearchTypes.FindAsync(id);

            if(searchType == null)
            {
                return NotFound();
            }


            searchType.Name = searchTypeDTO.Name;

            try
            {
                var result = _context.Update(searchType);
                await _context.SaveChangesAsync();

                return Ok(result.Entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SearchTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/SearchTypes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("line/{id}")]
        public async Task<IActionResult> PutSearchTypeLine(Guid id, SearchTypeLineDTO searchTypeLineDTO)
        {
            var searchTypeLine = await _context.SearchTypesLine.FindAsync(id);

            if (searchTypeLine == null)
            {
                return NotFound();
            }


            searchTypeLine.Name = searchTypeLineDTO.Name;

            try
            {
                var result = _context.SearchTypesLine.Update(searchTypeLine);
                await _context.SaveChangesAsync();

                return Ok(result.Entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SearchTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SearchTypes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SearchType>> PostSearchType(SearchTypeDTO searchTypeDTO)
        {
            if (_context.SearchTypes == null)
            {
                return Problem("Entity set 'ApplicationDbContext.SearchTypes'  is null.");
            }

            var searchType = new SearchType()
            {
                Name= searchTypeDTO.Name
            };

            _context.SearchTypes.Add(searchType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSearchType", new { id = searchType.Id }, searchType);
        }

        // POST: api/SearchTypes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("line/{id}")]
        public async Task<ActionResult<SearchType>> PostSearchTypeLine(Guid id, SearchTypeLineDTO searchTypelineDTO)
        {
            if (_context.SearchTypes == null)
            {
                return Problem("Entity set 'ApplicationDbContext.SearchTypes'  is null.");
            }
           
            var searchTypeLine = new SearchTypeLine()
            {
                Name= searchTypelineDTO.Name,
                SearchTypeId = id
            };

            _context.SearchTypesLine.Add(searchTypeLine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSearchTypeLine", new { id = searchTypeLine.Id }, searchTypeLine);
        }

        // DELETE: api/SearchTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSearchType(Guid id)
        {
            if (_context.SearchTypes == null)
            {
                return NotFound();
            }
            var searchType = await _context.SearchTypes.FindAsync(id);
            if (searchType == null)
            {
                return NotFound();
            }

            _context.SearchTypes.Remove(searchType);
            await _context.SaveChangesAsync();

            return Ok(id);
        }

        // DELETE: api/SearchTypes/5
        [HttpDelete("line/{id}")]
        public async Task<IActionResult> DeleteSearchTypeLine(Guid id)
        {
            if (_context.SearchTypes == null)
            {
                return NotFound();
            }
            var searchTypeLine = await _context.SearchTypesLine.FindAsync(id);
            if (searchTypeLine == null)
            {
                return NotFound();
            }

            _context.SearchTypesLine.Remove(searchTypeLine);
            await _context.SaveChangesAsync();

            return Ok(id);
        }

        private bool SearchTypeExists(Guid id)
        {
            return (_context.SearchTypes?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
