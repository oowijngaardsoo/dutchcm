﻿using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp;
using Dutchcm.Web.Data.Entities;
using SixLabors.ImageSharp.Processing;
using Dutchcm.Web.Models.Image;

namespace Dutchcm.Web.Helpers
{
    public static class ImageHelper
    {
        public static Data.Entities.Image ResizeImage(ImageUpdateDTO imageDTO)
        {
            using var ms = new MemoryStream();
            using var imageStream = imageDTO.File.OpenReadStream();
            IImageDecoder imageDecoder;

            if (imageDTO.File.ContentType == "image/jpeg" || imageDTO.File.ContentType == "image/jpg")
            {
                imageDecoder = new SixLabors.ImageSharp.Formats.Jpeg.JpegDecoder();
            }
            else if (imageDTO.File.ContentType == "image/png")
            {
                imageDecoder = new SixLabors.ImageSharp.Formats.Png.PngDecoder();
            }
            else
            {
                throw new Exception("");
            }

            if (imageStream.Position == imageStream.Length) //Check this because if your image is a .png, it might just throw an error
            {
                imageStream.Position = imageStream.Seek(0, SeekOrigin.Begin);
            }

            Image<SixLabors.ImageSharp.PixelFormats.Rgba32> imageSharp = imageDecoder.Decode<SixLabors.ImageSharp.PixelFormats.Rgba32>(Configuration.Default, imageStream, new CancellationToken());

            var width = (decimal)imageSharp.Width;
            var height = (decimal)imageSharp.Height;

            if (imageSharp.Height > 500)
            {
                decimal ratio = height / width;
                var newWidth = (int)Math.Ceiling(width * ratio);
                width = newWidth;
                height = 500;

                imageSharp.Mutate(i =>
                {
                    i.Resize((int)width, (int)height);
                });
            }

            imageSharp.SaveAsJpeg(ms, new SixLabors.ImageSharp.Formats.Jpeg.JpegEncoder() { Quality = 70 });
            var fileBytes = ms.ToArray();

            return new Data.Entities.Image()
            {
                FileName = $"{Guid.NewGuid()}-{imageDTO.File.FileName}",
                ImageTitle = $"{imageDTO.File.ContentType}",
                ImageData = Convert.ToBase64String(fileBytes),
                Created = DateTime.Now,
                CreatedBy = "",
                Height = (int)height,
                Width = (int)width,
            };

        }
    }
}
