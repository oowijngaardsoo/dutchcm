﻿using Dutchcm.Web.Models;
using Dutchcm.Web.Models.Requests;
using Microsoft.EntityFrameworkCore;

namespace Dutchcm.Web.Helpers
{
    public static class PageHelper
    {
        public static async Task<PagedResult<T>> GetPagedAsync<T>(this IQueryable<T> query,
                                         PagedRequest pagedRequest) where T : class
        {
            var result = new PagedResult<T>();
            result.CurrentPage = pagedRequest.Page;
            result.PageSize = pagedRequest.Amount;
            result.RowCount = query.Count();


            var pageCount = (double) result.RowCount / pagedRequest.Amount;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (pagedRequest.Page - 1) * pagedRequest.Amount;
            result.Results = await query.Skip(skip).Take(pagedRequest.Amount).ToListAsync();

            return result;
        }
    }
}
